package co.com.pragma.msapppragma;

import java.io.IOException;
import java.util.Date;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.DBConf;
import co.com.pragma.pojo.RequestResponse;

public class MsClienteCreate implements RequestHandler<SQSEvent, RequestResponse> {

	@Override
	public RequestResponse handleRequest(SQSEvent event, Context context) {
		ObjectMapper jsMapper = new ObjectMapper();
		RequestResponse rr = new RequestResponse();
		for (SQSMessage msg : event.getRecords()) {
			try {
				ClienteItem c = jsMapper.readValue((String) msg.getBody(), ClienteItem.class);
				DynamoDBMapper mapper = new DynamoDBMapper(DBConf.CLIENT);
				c.setId((new Date()).getTime());
				mapper.save(c);
				rr.setStatus(200);

				rr.setMessage("Registro creado con exito! " + c.toString());
			} catch (JsonParseException e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			} catch (JsonMappingException e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			} catch (IOException e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			} catch (AmazonServiceException e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			} catch (AmazonClientException e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			} catch (Exception e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			}
		}
		return rr;
	}

}
